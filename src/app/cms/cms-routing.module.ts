import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchPostsComponent } from './pages/search-posts/search-posts.component';
import { DoubleComponent } from './pages/double/double.component';


const routes: Routes = [
  {
    path: '',
    resolve: {},
    children: [
      {
        path: '',
        redirectTo: 'search-posts', pathMatch: 'full'
      },
      {
        path: 'search-posts',
        component: SearchPostsComponent
      },
      {
        path: 'search-double',
        component: DoubleComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmsRoutingModule { }
