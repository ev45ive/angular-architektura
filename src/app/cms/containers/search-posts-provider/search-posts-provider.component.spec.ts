import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPostsProviderComponent } from './search-posts-provider.component';

describe('SearchPostsProviderComponent', () => {
  let component: SearchPostsProviderComponent;
  let fixture: ComponentFixture<SearchPostsProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPostsProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPostsProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
