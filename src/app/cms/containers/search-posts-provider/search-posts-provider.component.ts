import { Component, OnInit, Input } from '@angular/core';
import { PostsStoreService } from '../../common/posts-store.service';

@Component({
  selector: 'app-search-posts-provider',
  template: '<ng-content></ng-content>',
  providers: [
    PostsStoreService
  ]
})
export class SearchPostsProviderComponent implements OnInit {

  @Input() set id(query) {
    if (query) {
      this.store.searchPosts(query)
    }
  }

  constructor(private store: PostsStoreService) { }

  ngOnInit() {
  }

}
