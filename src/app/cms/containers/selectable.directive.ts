import { Directive, HostListener, HostBinding, Input } from '@angular/core';
import { SelectionService } from './selection.service';

@Directive({
  selector: '[appSelectable]'
})
export class SelectableDirective {

  @Input() appSelectable: any;

  @HostListener('click')
  select() {
    
    this.selection.setSelected(this.appSelectable)
  }

  @HostBinding('class.table-active')
  get isSelected() {
    return this.selection.selected == this.appSelectable
  }


  constructor(
    public selection: SelectionService
  ) { 
    console.log(this)
  }

}
