import { Directive, Output, EventEmitter } from '@angular/core';
import { SelectionService } from './selection.service';
import { Class } from 'estree';

@Directive({
  selector: '[appSelection]',
  exportAs: 'mySelection',
  providers: [
    SelectionService
  ]
})
export class SelectionDirective {

  @Output() selectionChange = new EventEmitter<Class>();

  constructor(
    public selection: SelectionService
  ) {
    console.log(this)
  }

  getSelected() {
    return this.selection.selected
  }
}
