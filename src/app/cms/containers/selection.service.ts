import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SelectionService {

  selected:any

  setSelected(selected){
    this.selected = selected
  }

  constructor() { }
}
