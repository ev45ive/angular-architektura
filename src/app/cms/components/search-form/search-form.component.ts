import { Component, OnInit, ViewChild, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import { NgForm, FormBuilder, DefaultValueAccessor, FormControl } from '@angular/forms';
import { filter, debounceTime } from 'rxjs/operators'

DefaultValueAccessor

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Input() set query(q: string) {
    this.form.get('query').setValue(q, {
      emitEvent: false
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    changes['query'].currentValue
  }

  constructor(private fb: FormBuilder) {
    console.log(this.form)
  }

  form = this.fb.group({
    query: [''],
    address: new FormControl({
      fname: '', email: ''
    }),
    options: this.fb.group({

    })
  })

  ngOnInit() {
    this.form.get('query').valueChanges.pipe(
      debounceTime(400),
      filter(q => q.length >= 3),
    )
      .subscribe(q => this.search(q))
  }

  @Output() searchChange = new EventEmitter<string>();

  search(query: string) {
    this.searchChange.emit(query)
  }

}
