import { Component, OnInit, EventEmitter } from '@angular/core';
import { PostsStoreService } from '../../common/posts-store.service';
import { Post } from '../../common/Post';
import { Observable, ConnectableObservable, Subject, ReplaySubject, BehaviorSubject, Subscription } from 'rxjs';
import { tap, multicast, refCount, share, shareReplay, takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-posts',
  templateUrl: './search-posts.component.html',
  styleUrls: ['./search-posts.component.scss']
})
export class SearchPostsComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private postsStore: PostsStoreService) {
    
      // this.route.parent.params
  }

  results: Post[] = []

  resultsChange = this.postsStore.latestSearchResults.pipe(
    tap(results => this.results = results)
  )
  queryChange = this.postsStore.latestSearchQuery

  search(query) {
    this.postsStore.searchPosts(query)
  }

  ngOnInit() {

    this.search('sunt')
  }

}
