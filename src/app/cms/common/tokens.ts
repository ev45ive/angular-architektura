import { InjectionToken } from '@angular/core';
import { environment } from '../../../environments/environment';

// export abstract class PostAPIServiceURL{}

export const PostAPIServiceURL = new InjectionToken<string>('Post Api Service URL',
    {
        providedIn: 'root',
        factory: () => environment.api_url + 'posts/'
    });
