import { Injectable } from '@angular/core';
import { PostsApiService } from './posts-api.service';
import { PostApiServicesModule } from '../api-services/api-services.module';
import { Subject, BehaviorSubject, empty } from 'rxjs';
import { Post } from './Post';
import { distinctUntilChanged, map, switchAll, switchMap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: PostApiServicesModule
})
export class PostsStoreService {
  private errors = new Subject()

  private searchQuery = new BehaviorSubject<string>('batman');
  latestSearchQuery = this.searchQuery.pipe(
    distinctUntilChanged()
  )

  private searchResults = new BehaviorSubject<Post[]>([]);
  latestSearchResults = this.searchResults.asObservable()

  constructor(private api: PostsApiService) {

    this.latestSearchQuery.pipe(
      switchMap(query => this.api.searchPosts(query).pipe(
        catchError(err => {
          this.errors.next(err)
          return empty()
        })
      ))
    )
      .subscribe(results => {
        this.searchResults.next(results)
      })
  }

  searchPosts(query: any) {
    this.searchQuery.next(query)
  }

  handleCommand(postsCommands: any /* PostCommandsType */) {
    switch (postsCommands.type) {
      //
    }
  }

}
