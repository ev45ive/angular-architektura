import { Injectable, Inject } from '@angular/core';

import { HttpClient, HttpRequest, HttpErrorResponse } from '@angular/common/http'
import { PostAPIServiceURL } from './tokens';
import { PostApiServicesModule } from '../api-services/api-services.module';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Post } from './Post';

export class NoResultsFound extends Error { }

@Injectable({
  providedIn: PostApiServicesModule
  // providedIn: 'root',
})
export class PostsApiService {

  constructor(
    @Inject(PostAPIServiceURL)
    private api_url: string,
    private http: HttpClient) { }

  searchPosts(query: string) {
    return this.http.get<Post[]>(this.api_url, {
      params: {
        q: query
      },
      // reportProgress:true,
      // responseType:'arraybuffer',
      // observe:'response'
    }).pipe(
      catchError((err) => {
        if (err instanceof HttpErrorResponse && err.status == 404) {
          return throwError(new NoResultsFound(err.message))
        }
        return throwError(err)
      })
    )
  }

}
