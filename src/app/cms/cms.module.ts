import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CmsRoutingModule } from './cms-routing.module';
import { SearchPostsComponent } from './pages/search-posts/search-posts.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { PostsListComponent } from './components/posts-list/posts-list.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { PostApiServicesModule } from './api-services/api-services.module';
import { PostsStoreService } from './common/posts-store.service';
import { DoubleComponent } from './pages/double/double.component';
import { SearchPostsProviderComponent } from './containers/search-posts-provider/search-posts-provider.component';
import { SearchProviderDirective } from './containers/search-provider.directive';
import { SelectionDirective } from './containers/selection.directive';
import { SelectableDirective } from './containers/selectable.directive';
import { CustomFormGroupComponent } from './components/custom-form-group/custom-form-group.component';

@NgModule({
  declarations: [SearchPostsComponent, SearchFormComponent, PostsListComponent, DoubleComponent, SearchPostsProviderComponent, SearchProviderDirective, SelectionDirective, SelectableDirective, CustomFormGroupComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    CmsRoutingModule,
    HttpClientModule,
    PostApiServicesModule
  ],
  providers: [
    // {
    //   provide: MyHttpClient,
    //   useClass: HttpClient
    // },
    // {
    //   provide:HttpHandler,
    //   useClass: MySpecialMagicalHttpHandler
    // },
    // {
    //   provide: PostAPIServiceURL,
    //   useValue: environment.api_url + 'posts/'
    // },
    // {
    //   provide: PostsApiService,
    //   useFactory(url, http) {
    //     return new PostsApiService(url, http)
    //   },
    //   deps: [PostAPIServiceURL, HttpClient]
    // },
    // {
    //   provide: PostsApiService,
    //   useClass: PostsApiService,
    //   // deps: [PostAPIServiceURL, HttpClient]
    // },
    // PostsApiService
  ]
})
export class CmsModule {

  constructor(
    @Optional() @SkipSelf() private posts: PostsStoreService) {
    if (posts) {
      throw 'Please do not use Posts Service outside CMS module. Thank you! ;-) '
    }
  }
}
