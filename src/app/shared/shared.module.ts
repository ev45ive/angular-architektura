import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollapsibleComponent } from './collapsible/collapsible.component';
import { AccordionComponent } from './accordion/accordion.component';
import { AccordionTabComponent } from './accordion-tab/accordion-tab.component';



@NgModule({
  declarations: [CollapsibleComponent, AccordionComponent, AccordionTabComponent],
  imports: [
    CommonModule
  ],
  exports: [CollapsibleComponent, AccordionComponent, AccordionTabComponent]
})
export class SharedModule { }
