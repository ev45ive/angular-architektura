import { Component, OnInit, QueryList, ContentChildren } from '@angular/core';
import { AccordionTabComponent } from '../accordion-tab/accordion-tab.component';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {

  @ContentChildren(AccordionTabComponent)
  tabs: QueryList<AccordionTabComponent>

  open(tab: AccordionTabComponent) {
    this.tabs.forEach(tab => tab.open = false)
    tab.open = true
  }

  constructor() { }

  ngOnInit() {

  }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    
    this.tabs.changes.subscribe(tabs => {

      this.tabs.forEach(tab => {
        tab.openChange.subscribe(() => {
          this.open(tab)
        })
      })
    })

  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.

  }


}
