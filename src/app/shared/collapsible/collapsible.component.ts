import { Component, OnInit, forwardRef, EventEmitter } from '@angular/core';
import { AccordionTabComponent } from '../accordion-tab/accordion-tab.component';

@Component({
  selector: 'app-collapsible',
  templateUrl: './collapsible.component.html',
  styleUrls: ['./collapsible.component.scss'],
  providers: [
    {
      provide: AccordionTabComponent,
      useExisting: forwardRef(() => CollapsibleComponent)
    }
  ]
})
export class CollapsibleComponent extends AccordionTabComponent {



  constructor() {
    super()
  }

  ngOnInit() {
  }

}
