import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { TagPlaceholder } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-accordion-tab',
  templateUrl: './accordion-tab.component.html',
  styleUrls: ['./accordion-tab.component.scss']
})
export class AccordionTabComponent implements OnInit {

  @Input()
  label: string = ''

  open = false

  openChange = new EventEmitter<boolean>()

  toggle() {
    this.openChange.emit()
  }

  constructor() {
    // parent.registerTab(this)
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

}
