import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StyleguideRoutingModule } from './styleguide-routing.module';
import { TabsExamplesComponent } from './pages/tabs-examples/tabs-examples.component';
import { SharedModule } from '../shared/shared.module';
import { AccordionExampleComponent } from './accordion-example/accordion-example.component';


@NgModule({
  declarations: [TabsExamplesComponent, AccordionExampleComponent],
  imports: [
    CommonModule,
    StyleguideRoutingModule,
    SharedModule
  ]
})
export class StyleguideModule { }
