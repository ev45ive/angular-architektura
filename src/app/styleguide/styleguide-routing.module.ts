import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabsExamplesComponent } from './pages/tabs-examples/tabs-examples.component';
import { AccordionExampleComponent } from './accordion-example/accordion-example.component';


const routes: Routes = [
  {
    path: 'styleguide',
    children: [
      {
        path: '', redirectTo: 'accordion', pathMatch:'full'
      },
      {
        path: 'accordion',
        component: AccordionExampleComponent
      },
      {
        path: 'tabs',
        component: TabsExamplesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StyleguideRoutingModule { }
