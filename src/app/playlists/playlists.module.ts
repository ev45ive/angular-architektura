import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './pages/playlists/playlists.component';
import { ItemsListComponent } from './components/items-list/items-list.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistFormComponent } from './components/playlist-form/playlist-form.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [PlaylistsComponent, ItemsListComponent, ListItemComponent, PlaylistDetailsComponent, PlaylistFormComponent],
  imports: [
    FormsModule,
    CommonModule,
    PlaylistsRoutingModule
  ]
})
export class PlaylistsModule { }
