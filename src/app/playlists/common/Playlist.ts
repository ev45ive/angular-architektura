
interface Entity {
    id: number
    name: string
}

interface Playlist extends Entity {
    favorite: boolean
    /**
     * HEX Color
     */
    color: string
    /**
     * JSON ISO Date
     */
    createdAt: string
    tracks?: Track[]
}

interface Track extends Entity {
}

// const p: Playlist = {
//     id: 123,
//     favorite: true,
//     name: 'Angular Hits',
//     color: '#ff00ff',
//     createdAt: '2019-07-22T12:29:11.604Z',
// };
