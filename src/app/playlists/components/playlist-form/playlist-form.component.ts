import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss']
})
export class PlaylistFormComponent implements OnInit {

  static uid = 0
  uid = 'playlist_form_' + PlaylistFormComponent.uid++

  @Input()
  playlist: Playlist

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  constructor() { }

  ngOnInit() {
  }

}
