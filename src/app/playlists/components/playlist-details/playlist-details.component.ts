import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist: Playlist

  @Output() edit = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  helpers = {
    // html helpers...
  }

}
