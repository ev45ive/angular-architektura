import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { NgForOfContext } from '@angular/common';

NgForOfContext

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemsListComponent implements OnInit {

  @Input()
  selected: Playlist | null

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(selected: Playlist) {
    // this.selected = selected
    this.selectedChange.emit(selected)
  }

  @Input()
  playlists: Playlist[] = []

  trackFn(index, item) {
    return item.id
  }

  constructor() { }

  ngOnInit() {
  }

}
