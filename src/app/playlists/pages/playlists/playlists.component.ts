import { Component, OnInit, enableProdMode } from '@angular/core';

enum Modes {
  edit = 'edit',
  show = 'show'
};

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {

  Modes = Modes

  mode: Modes = Modes.show

  constructor() { }

  edit() {
    this.mode = Modes.edit
  }

  cancel() {
    this.mode = Modes.show
  }

  playlists: Playlist[] = [
    {
      id: 123,
      name: 'Angular Hits',
      favorite: true,
      color: '#ff00ff',
      createdAt: '2019-07-22T12:29:11.604Z',
    },
    {
      id: 234,
      name: 'Angular TOP20',
      favorite: true,
      color: '#ff00ff',
      createdAt: '2019-07-22T12:29:11.604Z',
    },
    {
      id: 345,
      name: 'Best of Angular',
      favorite: true,
      color: '#ff00ff',
      createdAt: '2019-07-22T12:29:11.604Z',
    }
  ]

  ngOnInit() {
  }

}
