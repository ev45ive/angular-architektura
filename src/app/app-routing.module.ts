import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules, RouteReuseStrategy } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'cms',
    pathMatch: 'full',
  },
  {
    path: 'cms',
    loadChildren() {
      return import('./cms/cms.module').then(({ CmsModule }) => CmsModule)
    },
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    paramsInheritanceStrategy: 'emptyOnly',
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
