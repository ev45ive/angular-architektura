import { TestBed } from '@angular/core/testing';

import { UiToolkitService } from './ui-toolkit.service';

describe('UiToolkitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UiToolkitService = TestBed.get(UiToolkitService);
    expect(service).toBeTruthy();
  });
});
