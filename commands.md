
git clone https://bitbucket.org/ev45ive/angular-architektura.git


 ng g m playlists -m app --routing true
 ng g c playlists/pages/playlists
 
 ng g c playlists/components/items-list
 ng g c playlists/components/list-item
 ng g c playlists/components/playlist-details
 ng g c playlists/components/playlist-form
 
npm install bootstrap --save


ng g m shared -m app

ng g m styleguide -m app --routing true

ng g c styleguide/pages/tabs-examples

ng g c shared/collapsible --export

npm install -g json-server

json-server https://jsonplaceholder.typicode.com/db 

ng g m cms -m app --routing true
